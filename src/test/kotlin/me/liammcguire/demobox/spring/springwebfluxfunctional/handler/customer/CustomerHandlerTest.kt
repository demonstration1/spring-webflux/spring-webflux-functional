package me.liammcguire.demobox.spring.springwebfluxfunctional.handler.customer

import com.mongodb.client.result.DeleteResult
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import me.liammcguire.demobox.spring.springwebfluxfunctional.configuration.RoutesConfiguration
import me.liammcguire.demobox.spring.springwebfluxfunctional.model.customer.Address
import me.liammcguire.demobox.spring.springwebfluxfunctional.model.customer.Customer
import me.liammcguire.demobox.spring.springwebfluxfunctional.repository.customer.CustomerRepository
import me.liammcguire.demobox.spring.springwebfluxfunctional.validation.customer.CustomerValidator
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest
import org.springframework.http.MediaType
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.reactive.server.WebTestClient
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@ExtendWith(SpringExtension::class)
@WebFluxTest(CustomerHandler::class)
@TestInstance(TestInstance.Lifecycle.PER_METHOD)
class CustomerHandlerTest {

    private val mockCustomerRepository: CustomerRepository = mockk()
    private val handler: CustomerHandler = CustomerHandler(mockCustomerRepository, CustomerValidator())

    private val webTestClient: WebTestClient = WebTestClient
        .bindToRouterFunction(RoutesConfiguration.customerHandlerRoutes(handler))
        .build()

    @Test
    @Suppress("ReactorUnusedPublisher")
    fun `Creating a new Customer should return a 200 response`() {
        val storedCustomer = createCustomer().copy(_id = null)
        every { mockCustomerRepository.store(storedCustomer) } returns Mono.just(storedCustomer.copy(
            _id = "5e2234051dfd0b58632c48ed"
        ))

        val responseBody = webTestClient.post()
            .uri("/api/customer/create")
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(storedCustomer)
            .exchange()
            .expectStatus().isCreated
            .expectBody(Customer::class.java)
            .returnResult()
            .responseBody

        assertNotNull(responseBody)
        val customer = responseBody!!
        assertEquals("5e2234051dfd0b58632c48ed", customer._id)
        assertEquals("John", customer.forename)
        assertEquals("Smith", customer.surname)
        assertEquals("john.smith@localhost.com", customer.emailAddress)
        assertEquals("07123456789", customer.contactNumber)

        val address = customer.address
        assertEquals("123 Sesame Street", address?.street)
        assertEquals("Manhattan", address?.city)
        assertEquals("NE66 1FE", address?.postcode)

        verify(exactly = 1) { mockCustomerRepository.store(storedCustomer) }
    }

    @Test
    @Suppress("ReactorUnusedPublisher")
    fun `Creating a new Customer should return a 400 response when an inval_id request is made`() {
        val newCustomer = createCustomer().copy(
            forename = null, surname = "£\$DD£\$DAS", address = null
        )

        webTestClient.post()
            .uri("/api/customer/create")
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(newCustomer)
            .exchange()
            .expectStatus().isBadRequest

        verify(exactly = 0) { mockCustomerRepository.store(any()) }
    }

    @Test
    @Suppress("ReactorUnusedPublisher")
    fun `Listing all Customers should return a 200 response`() {
        every { mockCustomerRepository.list(Customer::class) } returns Flux.fromArray(arrayOf(createCustomer()))

        val responseBody = webTestClient.get()
            .uri("/api/customer/list")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isOk
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBodyList(Customer::class.java)
            .hasSize(1)
            .returnResult()
            .responseBody

        assertNotNull(responseBody)
        val customer = responseBody!!.first()
        assertEquals("5e2234051dfd0b58632c48ed", customer._id)
        assertEquals("John", customer.forename)
        assertEquals("Smith", customer.surname)
        assertEquals("john.smith@localhost.com", customer.emailAddress)
        assertEquals("07123456789", customer.contactNumber)

        val address = customer.address
        assertEquals("123 Sesame Street", address?.street)
        assertEquals("Manhattan", address?.city)
        assertEquals("NE66 1FE", address?.postcode)

        verify(exactly = 1) { mockCustomerRepository.list(Customer::class) }
    }

    @Test
    @Suppress("ReactorUnusedPublisher")
    fun `Listing all Customers should return a 404 response when no Customers could be found`() {
        every { mockCustomerRepository.list(Customer::class) } returns Flux.empty()

        webTestClient.get()
            .uri("/api/customer/list")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isNotFound
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .isEmpty

        verify(exactly = 1) { mockCustomerRepository.list(Customer::class) }
    }

    @Test
    @Suppress("ReactorUnusedPublisher")
    fun `Viewing a Customer should return a 200 response`() {
        every {
            mockCustomerRepository.viewById("5e2234051dfd0b58632c48ed", Customer::class)
        } returns Mono.just(createCustomer())

        val responseBody = webTestClient.get()
            .uri("/api/customer/view/5e2234051dfd0b58632c48ed")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isOk
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBody(Customer::class.java)
            .returnResult()
            .responseBody

        assertNotNull(responseBody)
        val customer = responseBody!!
        assertEquals("5e2234051dfd0b58632c48ed", customer._id)
        assertEquals("John", customer.forename)
        assertEquals("Smith", customer.surname)
        assertEquals("john.smith@localhost.com", customer.emailAddress)
        assertEquals("07123456789", customer.contactNumber)

        val address = customer.address
        assertEquals("123 Sesame Street", address?.street)
        assertEquals("Manhattan", address?.city)
        assertEquals("NE66 1FE", address?.postcode)

        verify(exactly = 1) { mockCustomerRepository.viewById("5e2234051dfd0b58632c48ed", Customer::class) }
    }

    @Test
    @Suppress("ReactorUnusedPublisher")
    fun `Viewing a Customer should return a 404 response when a Customer couldn't be found`() {
        every {
            mockCustomerRepository.viewById("5e2234051dfd0b58632c48ed", Customer::class)
        } returns Mono.empty()

        webTestClient.get()
            .uri("/api/customer/view/5e2234051dfd0b58632c48ed")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isNotFound
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .isEmpty

        verify(exactly = 1) { mockCustomerRepository.viewById("5e2234051dfd0b58632c48ed", Customer::class) }
    }

    @Test
    @Suppress("ReactorUnusedPublisher")
    internal fun `Removing a Customer should return a 200 response`() {
        val storedCustomer = createCustomer()
        every {
            mockCustomerRepository.viewById("5e2234051dfd0b58632c48ed", Customer::class)
        } returns Mono.just(storedCustomer)
        every {
            mockCustomerRepository.remove(storedCustomer)
        } returns Mono.just(DeleteResult.acknowledged(1))

        webTestClient.delete()
            .uri("/api/customer/delete/5e2234051dfd0b58632c48ed")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isOk
            .expectBody()
            .isEmpty

        verify(exactly = 1) {
            mockCustomerRepository.viewById("5e2234051dfd0b58632c48ed", Customer::class)
            mockCustomerRepository.remove(storedCustomer)
        }
    }

    @Test
    @Suppress("ReactorUnusedPublisher")
    fun `Removing a Customer should return a 404 response when a Customer couldn't be found`() {
        every {
            mockCustomerRepository.viewById("5e2234051dfd0b58632c48ed", Customer::class)
        } returns Mono.empty()

        webTestClient.delete()
            .uri("/api/customer/delete/5e2234051dfd0b58632c48ed")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus().isNotFound
            .expectBody()
            .isEmpty

        verify(exactly = 1) { mockCustomerRepository.viewById("5e2234051dfd0b58632c48ed", Customer::class) }
        verify(exactly = 0) { mockCustomerRepository.remove(any()) }
    }

    @Test
    @Suppress("ReactorUnusedPublisher")
    fun `Updating an existing Customer should return a 200 response`() {
        val storedCustomer = createCustomer()
        every {
            mockCustomerRepository.viewById("5e2234051dfd0b58632c48ed", Customer::class)
        } returns Mono.just(createCustomer())
        every { mockCustomerRepository.store(any()) } returns Mono.just(storedCustomer.copy(
            emailAddress = "john.smith123@localhost.com",
            address = storedCustomer.address?.copy(
                street = "321 Sesame Street"
            )
        ))

        val responseBody = webTestClient.put()
            .uri("/api/customer/update/5e2234051dfd0b58632c48ed")
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(createCustomer())
            .exchange()
            .expectStatus().isOk
            .expectHeader().contentType(MediaType.APPLICATION_JSON)
            .expectBody(Customer::class.java)
            .returnResult()
            .responseBody

        assertNotNull(responseBody)
        val customer = responseBody!!
        assertEquals("5e2234051dfd0b58632c48ed", customer._id)
        assertEquals("John", customer.forename)
        assertEquals("Smith", customer.surname)
        assertEquals("john.smith123@localhost.com", customer.emailAddress)
        assertEquals("07123456789", customer.contactNumber)

        val address = customer.address
        assertEquals("321 Sesame Street", address?.street)
        assertEquals("Manhattan", address?.city)
        assertEquals("NE66 1FE", address?.postcode)

        verify(exactly = 1) {
            mockCustomerRepository.viewById("5e2234051dfd0b58632c48ed", Customer::class)
            mockCustomerRepository.store(any())
        }
    }

    @Test
    @Suppress("ReactorUnusedPublisher")
    fun `Updating an existing Customer should return a 404 response when a Customer couldn't be found`() {
        every {
            mockCustomerRepository.viewById("5e2234051dfd0b58632c48ed", Customer::class)
        } returns Mono.empty()

        webTestClient.put()
            .uri("/api/customer/update/5e2234051dfd0b58632c48ed")
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(createCustomer())
            .exchange()
            .expectStatus().isNotFound
            .expectBody()
            .isEmpty

        verify(exactly = 1) { mockCustomerRepository.viewById("5e2234051dfd0b58632c48ed", Customer::class) }
        verify(exactly = 0) { mockCustomerRepository.store(any()) }
    }

    @Test
    @Suppress("ReactorUnusedPublisher")
    fun `Updating an existing Customer should return a 400 response when an inval_id request is made`() {
        every {
            mockCustomerRepository.viewById("5e2234051dfd0b58632c48ed", Customer::class)
        } returns Mono.just(createCustomer())
        val updateCustomer = createCustomer().copy(
            surname = "£\$DD£\$DAS", address = Address(street = "&*&£*$&")
        )

        webTestClient.put()
            .uri("/api/customer/update/5e2234051dfd0b58632c48ed")
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(updateCustomer)
            .exchange()
            .expectStatus().isBadRequest

        verify(exactly = 0) { mockCustomerRepository.store(any()) }
    }

    companion object CustomerObjects {
        fun createCustomer() = Customer(
            _id = "5e2234051dfd0b58632c48ed",
            forename = "John",
            surname = "Smith",
            emailAddress = "john.smith@localhost.com",
            contactNumber = "07123456789",
            address = Address(
                street = "123 Sesame Street",
                city = "Manhattan",
                postcode = "NE66 1FE"
            )
        )
    }
}