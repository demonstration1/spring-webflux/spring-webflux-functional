package me.liammcguire.demobox.spring.springwebfluxfunctional.repository

import com.mongodb.client.result.DeleteResult
import me.liammcguire.demobox.spring.springwebfluxfunctional.model.StoreableModel
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import kotlin.reflect.KClass

abstract class Repository<SM : StoreableModel>(
    private val reactiveMongoTemplate: ReactiveMongoTemplate
) {
    fun store(model: SM): Mono<SM> = reactiveMongoTemplate.save(model)

    fun list(type: KClass<SM>): Flux<SM> = reactiveMongoTemplate.findAll(type.java)

    fun viewById(id: String, type: KClass<SM>): Mono<SM> = reactiveMongoTemplate.findById(id, type.java)

    fun remove(model: SM): Mono<DeleteResult> = reactiveMongoTemplate.remove(model)
}
