package me.liammcguire.demobox.spring.springwebfluxfunctional.configuration

import me.liammcguire.demobox.spring.springwebfluxfunctional.handler.customer.CustomerHandler
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.server.RouterFunction
import org.springframework.web.reactive.function.server.ServerResponse
import org.springframework.web.reactive.function.server.router

object RoutesConfiguration {

    fun customerHandlerRoutes(customerHandler: CustomerHandler): RouterFunction<ServerResponse> = router {
        "/api/customer".nest {
            GET("/list", customerHandler::list)
            GET("/view/{id}", customerHandler::view)
            DELETE("/delete/{id}", customerHandler::delete)
            accept(MediaType.APPLICATION_JSON).nest {
                POST("/create", customerHandler::create)
                PUT("/update/{id}", customerHandler::update)
            }
        }
    }
}