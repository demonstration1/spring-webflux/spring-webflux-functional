package me.liammcguire.demobox.spring.springwebfluxfunctional.handler

import me.liammcguire.demobox.spring.springwebfluxfunctional.model.StoreableModel
import me.liammcguire.demobox.spring.springwebfluxfunctional.repository.Repository
import me.liammcguire.demobox.spring.springwebfluxfunctional.utility.ResponseBuilder.buildResponse
import org.springframework.http.HttpStatus
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono
import kotlin.reflect.KClass

interface AsyncView<SM : StoreableModel> {
    val repository: Repository<SM>
    val storeableType: KClass<SM>

    fun view(request: ServerRequest): Mono<ServerResponse> = repository
        .viewById(request.pathVariable("id"), storeableType)
        .flatMap { model -> buildResponse(HttpStatus.OK, model) }
        .switchIfEmpty(buildResponse<SM>(HttpStatus.NOT_FOUND))
}
