package me.liammcguire.demobox.spring.springwebfluxfunctional.validation

import me.liammcguire.demobox.spring.springwebfluxfunctional.model.ValidatableModel
import me.liammcguire.demobox.spring.springwebfluxfunctional.utility.ResponseBuilder.buildResponse
import org.springframework.http.HttpStatus
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono

abstract class Validator<VM : ValidatableModel> {

    fun validate(model: VM, validateMandatoryFields: Boolean = true): List<ValidationFault> = validateFields(model).plus(
        when (validateMandatoryFields) {
            true -> validateMandatoryFields(model)
            false -> emptyList()
        }
    )

    protected abstract fun validateFields(model: VM): List<ValidationFault>

    protected abstract fun validateMandatoryFields(model: VM): List<ValidationFault>
}

data class ValidationFault(val field: String, val fault: String)

fun <VM : ValidatableModel> Mono<VM>.validate(
    validator: Validator<VM>,
    validateMandatoryFields: Boolean,
    onValid: (VM) -> Mono<ServerResponse>
): Mono<ServerResponse> = this.flatMap { model ->
    val validationResult = validator.validate(model, validateMandatoryFields)
    when (validationResult.isEmpty()) {
        true -> onValid(model)
        false -> buildResponse(HttpStatus.BAD_REQUEST, validateMandatoryFields)
    }
}