package me.liammcguire.demobox.spring.springwebfluxfunctional.validation

object ValidationRegex {
    const val REGEX_ALPHA_WITH_SPACE: String = "^[a-zA-Z ]+\$"
    const val REGEX_ALPHANUMERIC_WITH_SPACE: String = "^[a-zA-Z0-9 ]+\$"

    const val REGEX_MOBILE_NUMBER: String = "((\\+44(\\s\\(0\\)\\s|\\s0\\s|\\s)?)|0)7\\d{3}(\\s)?\\d{6}"
    const val REGEX_POST_CODE: String = "(GIR 0AA)|((([A-Z-[QVX]][0-9][0-9]?)|(([A-Z-[QVX]][A-Z-[IJZ]][0-9][0-9]?)|" +
        "(([A-Z-[QVX]][0-9][A-HJKSTUW])|([A-Z-[QVX]][A-Z-[IJZ]][0-9][ABEHMNPRVWXY]))))\\s?[0-9][A-Z-[CIKMOV]]{2})"
}
