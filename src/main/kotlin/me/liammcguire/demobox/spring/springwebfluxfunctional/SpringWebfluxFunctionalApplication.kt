package me.liammcguire.demobox.spring.springwebfluxfunctional

import me.liammcguire.demobox.spring.springwebfluxfunctional.configuration.BeansConfiguration
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SpringWebfluxFunctionalApplication

fun main(args: Array<String>) {
    runApplication<SpringWebfluxFunctionalApplication>(*args) {
        addInitializers(BeansConfiguration.beans())
    }
}