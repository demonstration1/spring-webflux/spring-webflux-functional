package me.liammcguire.demobox.spring.springwebfluxfunctional.model.customer

import me.liammcguire.demobox.spring.springwebfluxfunctional.model.StoreableModel
import me.liammcguire.demobox.spring.springwebfluxfunctional.model.ValidatableModel

data class Customer(
    override val _id: String? = null,
    val forename: String? = null,
    val surname: String? = null,
    val emailAddress: String? = null,
    val contactNumber: String? = null,
    val address: Address? = null
) : StoreableModel, ValidatableModel

data class Address(
    val street: String? = null,
    val city: String? = null,
    val postcode: String? = null
)