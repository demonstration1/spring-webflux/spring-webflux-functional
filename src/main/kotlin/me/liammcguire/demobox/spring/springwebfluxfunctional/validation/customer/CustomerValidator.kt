package me.liammcguire.demobox.spring.springwebfluxfunctional.validation.customer

import me.liammcguire.demobox.spring.springwebfluxfunctional.model.customer.Customer
import me.liammcguire.demobox.spring.springwebfluxfunctional.validation.ValidationFault
import me.liammcguire.demobox.spring.springwebfluxfunctional.validation.ValidationRegex.REGEX_ALPHANUMERIC_WITH_SPACE
import me.liammcguire.demobox.spring.springwebfluxfunctional.validation.ValidationRegex.REGEX_ALPHA_WITH_SPACE
import me.liammcguire.demobox.spring.springwebfluxfunctional.validation.ValidationRegex.REGEX_MOBILE_NUMBER
import me.liammcguire.demobox.spring.springwebfluxfunctional.validation.ValidationRegex.REGEX_POST_CODE
import me.liammcguire.demobox.spring.springwebfluxfunctional.validation.Validator
import java.util.regex.Pattern

class CustomerValidator : Validator<Customer>() {
    override fun validateFields(model: Customer): List<ValidationFault> = listOfNotNull(
        ValidationFault("forename", ERROR_FORENAME_LENGTH)
            .takeIf { model.forename != null && model.forename.length > 50 },
        ValidationFault("forename", ERROR_FORENAME_INVALID_CHARACTERS)
            .takeIf { model.forename != null && !Pattern.matches(REGEX_ALPHA_WITH_SPACE, model.forename) },
        ValidationFault("surname", ERROR_SURNAME_LENGTH)
            .takeIf { model.surname != null && model.surname.length > 50 },
        ValidationFault("surname", ERROR_SURNAME_INVALID_CHARACTERS)
            .takeIf { model.surname != null && !Pattern.matches(REGEX_ALPHA_WITH_SPACE, model.surname) },
        ValidationFault("contactNumber", ERROR_CONTACT_NUMBER_INVALID)
            .takeIf { model.contactNumber != null && !Pattern.matches(REGEX_MOBILE_NUMBER, model.contactNumber) },
        ValidationFault("address.street", ERROR_STREET_LENGTH)
            .takeIf { model.address?.street != null && model.address.street.length > 50 },
        ValidationFault("address.street", ERROR_STREET_INVALID_CHARACTERS)
            .takeIf { model.address?.street != null && !Pattern.matches(REGEX_ALPHANUMERIC_WITH_SPACE, model.address.street) },
        ValidationFault("address.city", ERROR_CITY_LENGTH)
            .takeIf { model.address?.city != null && model.address.city.length > 50 },
        ValidationFault("address.city", ERROR_CITY_INVALID_CHARACTERS)
            .takeIf { model.address?.city != null && !Pattern.matches(REGEX_ALPHA_WITH_SPACE, model.address.city) },
        ValidationFault("address.postcode", ERROR_POST_CODE_INVALID)
            .takeIf { model.address?.postcode != null && !Pattern.matches(REGEX_POST_CODE, model.address.postcode) }
    )

    override fun validateMandatoryFields(model: Customer): List<ValidationFault> = listOfNotNull(
        ValidationFault("forename", ERROR_FORENAME_MISSING)
            .takeIf { model.forename.isNullOrBlank() },
        ValidationFault("surname", ERROR_SURNAME_MISSING)
            .takeIf { model.surname.isNullOrBlank() },
        ValidationFault("emailAddress", ERROR_EMAIL_ADDRESS_MISSING)
            .takeIf { model.emailAddress.isNullOrBlank() },
        ValidationFault("contactNumber", ERROR_CONTACT_NUMBER_MISSING)
            .takeIf { model.contactNumber.isNullOrBlank() },
        ValidationFault("address", ERROR_ADDRESS_MISSING)
            .takeIf { model.address == null },
        ValidationFault("address.street", ERROR_STREET_MISSING)
            .takeIf { model.address?.street.isNullOrBlank() },
        ValidationFault("address.city", ERROR_CITY_MISSING)
            .takeIf { model.address?.city.isNullOrBlank() },
        ValidationFault("address.postcode", ERROR_POST_CODE_MISSING)
            .takeIf { model.address?.postcode.isNullOrBlank() }
    )

    companion object {
        private const val ERROR_FORENAME_MISSING: String =
            "A forename was expected but was not provided."
        private const val ERROR_FORENAME_LENGTH: String =
            "The provided forename exceeded the maximum allowed character length of 50."
        private const val ERROR_FORENAME_INVALID_CHARACTERS: String =
            "The provided forename contained invalid characters."

        private const val ERROR_SURNAME_MISSING: String =
            "A surname was expected but was not provided."
        private const val ERROR_SURNAME_LENGTH: String =
            "The provided surname exceeded the maximum allowed character length of 50."
        private const val ERROR_SURNAME_INVALID_CHARACTERS: String =
            "The provided surname contained invalid characters."

        private const val ERROR_EMAIL_ADDRESS_MISSING: String =
            "An email address was expected but was not provided."
        
        private const val ERROR_CONTACT_NUMBER_MISSING: String =
            "A contact number was expected but was not provided."
        private const val ERROR_CONTACT_NUMBER_INVALID: String =
            "The provided contact number was not valid."

        private const val ERROR_ADDRESS_MISSING: String =
            "An address was expected but was not provided."

        private const val ERROR_STREET_MISSING: String =
            "A street was expected but was not provided."
        private const val ERROR_STREET_LENGTH: String =
            "The provided street exceeded the maximum allowed character length of 50."
        private const val ERROR_STREET_INVALID_CHARACTERS: String =
            "The provided street contained invalid characters."

        private const val ERROR_CITY_MISSING: String =
            "A city was expected but was not provided."
        private const val ERROR_CITY_LENGTH: String =
            "The provided city exceeded the maximum allowed character length of 50."
        private const val ERROR_CITY_INVALID_CHARACTERS: String =
            "The provided city contained invalid characters."

        private const val ERROR_POST_CODE_MISSING: String =
            "A post code was expected but was not provided."
        private const val ERROR_POST_CODE_INVALID: String =
            "The provided post code was invalid."
    }
}