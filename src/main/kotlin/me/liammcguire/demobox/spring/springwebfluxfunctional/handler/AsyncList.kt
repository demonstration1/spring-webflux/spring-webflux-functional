package me.liammcguire.demobox.spring.springwebfluxfunctional.handler

import me.liammcguire.demobox.spring.springwebfluxfunctional.model.StoreableModel
import me.liammcguire.demobox.spring.springwebfluxfunctional.repository.Repository
import me.liammcguire.demobox.spring.springwebfluxfunctional.utility.ResponseBuilder.buildResponse
import org.springframework.http.HttpStatus
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono
import kotlin.reflect.KClass

interface AsyncList<SM : StoreableModel> {
    val repository: Repository<SM>
    val storeableType: KClass<SM>

    fun list(request: ServerRequest): Mono<ServerResponse> = repository.list(storeableType)
        .collectList()
        .flatMap { models ->
            when (models.isNotEmpty()) {
                true -> buildResponse<List<SM>>(HttpStatus.OK, models)
                false -> buildResponse<List<SM>>(HttpStatus.NOT_FOUND)
            }
        }
}
