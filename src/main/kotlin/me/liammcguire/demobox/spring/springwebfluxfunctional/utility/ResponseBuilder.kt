package me.liammcguire.demobox.spring.springwebfluxfunctional.utility

import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.web.reactive.function.BodyInserters
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono

object ResponseBuilder {
    fun <T : Any> buildResponse(status: HttpStatus, body: T? = null): Mono<ServerResponse> = body?.let {
        ServerResponse.status(status).contentType(MediaType.APPLICATION_JSON).body(BodyInserters.fromValue(body))
    } ?: ServerResponse.status(status).contentType(MediaType.APPLICATION_JSON).build()
}
