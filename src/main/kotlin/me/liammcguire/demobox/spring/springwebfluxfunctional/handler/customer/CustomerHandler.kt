package me.liammcguire.demobox.spring.springwebfluxfunctional.handler.customer

import me.liammcguire.demobox.spring.springwebfluxfunctional.handler.AsyncCreate
import me.liammcguire.demobox.spring.springwebfluxfunctional.handler.AsyncDelete
import me.liammcguire.demobox.spring.springwebfluxfunctional.handler.AsyncList
import me.liammcguire.demobox.spring.springwebfluxfunctional.handler.AsyncUpdate
import me.liammcguire.demobox.spring.springwebfluxfunctional.handler.AsyncView
import me.liammcguire.demobox.spring.springwebfluxfunctional.model.customer.Address
import me.liammcguire.demobox.spring.springwebfluxfunctional.model.customer.Customer
import me.liammcguire.demobox.spring.springwebfluxfunctional.repository.customer.CustomerRepository
import me.liammcguire.demobox.spring.springwebfluxfunctional.validation.customer.CustomerValidator
import kotlin.reflect.KClass

class CustomerHandler(
    override val repository: CustomerRepository,
    override val validator: CustomerValidator
) : AsyncCreate<Customer, Customer>,
    AsyncList<Customer>,
    AsyncView<Customer>,
    AsyncDelete<Customer>,
    AsyncUpdate<Customer, Customer> {

    override val validatableType: KClass<Customer> get() = Customer::class
    override val storeableType: KClass<Customer> get() = Customer::class

    override fun Customer.toStoreableModel(): Customer = this

    override fun Customer.mergeWith(validatedModel: Customer): Customer = this.copy(
        forename = validatedModel.forename.takeIf { !it.isNullOrBlank() } ?: this.forename,
        surname = validatedModel.surname.takeIf { !it.isNullOrBlank() } ?: this.surname,
        emailAddress = validatedModel.emailAddress.takeIf { !it.isNullOrBlank() } ?: this.emailAddress,
        contactNumber = validatedModel.contactNumber.takeIf { !it.isNullOrBlank() } ?: this.contactNumber,
        address = Address(
            street = validatedModel.address?.street.takeIf { !it.isNullOrBlank() } ?: this.address?.street,
            city = validatedModel.address?.city.takeIf { !it.isNullOrBlank() } ?: this.address?.city,
            postcode = validatedModel.address?.postcode.takeIf { !it.isNullOrBlank() } ?: this.address?.postcode
        )
    )
}