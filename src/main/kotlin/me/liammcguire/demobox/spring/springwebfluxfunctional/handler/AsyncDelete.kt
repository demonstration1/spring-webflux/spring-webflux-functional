package me.liammcguire.demobox.spring.springwebfluxfunctional.handler

import me.liammcguire.demobox.spring.springwebfluxfunctional.model.StoreableModel
import me.liammcguire.demobox.spring.springwebfluxfunctional.repository.Repository
import me.liammcguire.demobox.spring.springwebfluxfunctional.utility.ResponseBuilder.buildResponse
import org.springframework.http.HttpStatus
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono
import kotlin.reflect.KClass

interface AsyncDelete<SM : StoreableModel> {
    val repository: Repository<SM>
    val storeableType: KClass<SM>

    fun delete(request: ServerRequest): Mono<ServerResponse> = repository
        .viewById(request.pathVariable("id"), storeableType)
        .flatMap { storedModel ->
            repository.remove(storedModel).flatMap { deleteResult ->
                when (deleteResult.deletedCount == 1L) {
                    true -> buildResponse<Nothing>(HttpStatus.OK)
                    false -> buildResponse<Nothing>(HttpStatus.INTERNAL_SERVER_ERROR)
                }
            }
        }
        .switchIfEmpty(buildResponse<Nothing>(HttpStatus.NOT_FOUND))
}
