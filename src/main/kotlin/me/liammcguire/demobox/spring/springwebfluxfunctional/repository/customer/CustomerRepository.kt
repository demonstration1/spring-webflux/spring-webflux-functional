package me.liammcguire.demobox.spring.springwebfluxfunctional.repository.customer

import me.liammcguire.demobox.spring.springwebfluxfunctional.model.customer.Customer
import me.liammcguire.demobox.spring.springwebfluxfunctional.repository.Repository
import org.springframework.data.mongodb.core.ReactiveMongoTemplate

class CustomerRepository(
    reactiveMongoTemplate: ReactiveMongoTemplate
) : Repository<Customer>(reactiveMongoTemplate)