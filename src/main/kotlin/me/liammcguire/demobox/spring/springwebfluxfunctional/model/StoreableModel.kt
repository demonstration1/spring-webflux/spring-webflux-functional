package me.liammcguire.demobox.spring.springwebfluxfunctional.model

interface StoreableModel {
    val _id: String?
}
