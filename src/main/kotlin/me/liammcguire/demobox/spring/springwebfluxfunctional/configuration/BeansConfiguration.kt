package me.liammcguire.demobox.spring.springwebfluxfunctional.configuration

import com.mongodb.ConnectionString
import me.liammcguire.demobox.spring.springwebfluxfunctional.handler.customer.CustomerHandler
import me.liammcguire.demobox.spring.springwebfluxfunctional.repository.customer.CustomerRepository
import me.liammcguire.demobox.spring.springwebfluxfunctional.validation.customer.CustomerValidator
import org.springframework.context.support.BeanDefinitionDsl
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.core.SimpleReactiveMongoDatabaseFactory

object BeansConfiguration {

    private const val DB_CONNECTION_STRING: String = "mongodb://localhost:27017/spring-webflux-functional"

    fun beans(): BeanDefinitionDsl = org.springframework.context.support.beans {
        bean { ReactiveMongoTemplate(SimpleReactiveMongoDatabaseFactory(ConnectionString(DB_CONNECTION_STRING))) }

        bean { CustomerValidator() }
        bean { CustomerRepository(ref()) }
        bean { CustomerHandler(ref(), ref()) }

        bean { RoutesConfiguration.customerHandlerRoutes(ref()) }
    }
}