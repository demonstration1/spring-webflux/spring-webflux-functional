package me.liammcguire.demobox.spring.springwebfluxfunctional.handler

import me.liammcguire.demobox.spring.springwebfluxfunctional.model.StoreableModel
import me.liammcguire.demobox.spring.springwebfluxfunctional.model.ValidatableModel
import me.liammcguire.demobox.spring.springwebfluxfunctional.repository.Repository
import me.liammcguire.demobox.spring.springwebfluxfunctional.utility.ResponseBuilder.buildResponse
import me.liammcguire.demobox.spring.springwebfluxfunctional.validation.Validator
import me.liammcguire.demobox.spring.springwebfluxfunctional.validation.validate
import org.springframework.http.HttpStatus
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.ServerResponse
import reactor.core.publisher.Mono
import kotlin.reflect.KClass

interface AsyncUpdate<SM : StoreableModel, VM : ValidatableModel> {
    val repository: Repository<SM>
    val validator: Validator<VM>
    val storeableType: KClass<SM>
    val validatableType: KClass<VM>

    fun update(request: ServerRequest): Mono<ServerResponse> = request.bodyToMono(validatableType.java)
        .validate(validator, false) { validatedInModel ->
            repository.viewById(request.pathVariable("id"), storeableType)
                .flatMap { storedModel ->
                    repository.store(storedModel.mergeWith(validatedInModel)).flatMap { updatedModel ->
                        buildResponse(HttpStatus.OK, updatedModel)
                    }
                }
                .switchIfEmpty(buildResponse<SM>(HttpStatus.NOT_FOUND))
        }

    fun SM.mergeWith(validatedModel: VM): SM
}
